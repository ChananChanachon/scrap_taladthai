import os
import selenium
from selenium import webdriver
import time
import io
import requests
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import ElementClickInterceptedException

from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import pandas as pd
import numpy as np

path = os.getcwd()
url = 'https://talaadthai.com/product-search'
path_to_driver = os.getcwd() + '/chromedriver'

web = ['https://talaadthai.com/product-search/result?subcat_id=7324&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7326&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7331&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7334&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=4951&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7318&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7306&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=7310&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=10132&per_page=30&page=1'
      ,'https://talaadthai.com/product-search/result?subcat_id=10133&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=10134&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=7353&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=7364&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=10135&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=10136&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=7375&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=7398&per_page=30&page=1'
       ,'https://talaadthai.com/product-search/result?subcat_id=10130&per_page=30&page=1']

type_t = ['ตลาดผลไม้ฤดูกาล'
       ,'ตลาดผลไม้รวม'
       ,'ตลาดส้ม'
       ,'ตลาดแตงโม'
       ,'ตลาดผัก'
       ,'ตลาดผลไม้นานาชาติ และ ตลาดคอนเทนเนอร์'
       ,'ลานผัก(โซนผักปลีก)'
       ,'ตลาดผัก'
       ,'ตลาดสด'
       ,'ตลาดปลาน้ำจืด'
       ,'ตลาดอาหารทะเล'
       ,'ตลาดข้าวสาร'
       ,'ตลาดพืชไร่'
       ,'ตลาดอาหารแห้ง'
       ,'ตลาดสินค้าเบ็ดเตล็ด'
       ,'ตลาดต้นไม้'
       ,'ตลาดดอกไม้'
       ,'ตลาดสัตว์เลี้ยง'
       ]

talad_dict = {}
for num,value in enumerate(type_t):
    talad_dict[value] = web[num]
    
talad_list = list(talad_dict.keys())

for n_talad in range(len(talad_dict)):
    talad_type = talad_list[n_talad]
#     print(talad_type)
    talad_url = talad_dict[talad_type]
#     print(talad_url)

df_stack =  []

stack = []
for n_talad in range(len(talad_dict)):
    talad_type = talad_list[n_talad]
    talad_url = talad_dict[talad_type]
    print(talad_type)
    
    options = Options()
    options.add_argument("--headless")
    browser = webdriver.Chrome(executable_path = path_to_driver, chrome_options=options)
    browser.get(talad_url)
    time.sleep(4)
    
    select = Select(browser.find_element_by_xpath('//*[@id="__layout"]/div/div/div[2]/div[4]/div/div/div/div[2]/div[3]/div/div/div/select'))
    select.select_by_value("50")
    time.sleep(4)
    
    elems = browser.find_elements_by_class_name("pagination [href]")
    links = [elem.get_attribute('href') for elem in elems]
    links = list(np.unique(links))
    
    browser.close()
    time.sleep(4)
    
    for link in links:
        print(link)
        options = Options()
        options.add_argument("--headless")
        browser = webdriver.Chrome(executable_path = path_to_driver, chrome_options=options)
        browser.get(link)
        time.sleep(4)
        
        drop_page = browser.find_elements_by_xpath('//*[@id="__layout"]/div/div/div[2]/div[4]')
        data_i = drop_page[0].text.split('\n')

        head = [data_i.index(a) for a in data_i if a == ' 50 รายการ'][0]+1
        if 'Prev' in data_i:
            foot = data_i[1:].index('Prev')+1
        else:
            foot = data_i[1:].index('1')+1

        data = data_i[head:-(len(data_i) - foot)]
        
        columns_name = ['goods_and_product','lowest_threshold_price','highest_threshold_price','price_unit','measure_unit','previous_comparison','date']
        df = []
        num_range = [n for n in range(0,len(data),2)]
        for num,value in enumerate(num_range):
            datum = []
            if value != len(data):
                datum.append(data[value])
                l_list = [e for e in data[value+1].split(' ') if e not in ['–','/']]
                [datum.append(f) for f in l_list]
                df.append(datum)
            else:
                pass

        df = [f for f in df if len(f) == 7]
        fix = [g for g in df if len(g) != 7]
        for n in range(len(fix)):
            fix_i = [i for i in fix[n] if i != '-']
            fix_i[1] = 'ไม่พบราคา'
            fix_i[2] = 'ไม่พบราคา'

            df.append(fix_i)
        
        result = pd.DataFrame(df, columns = columns_name)
        result['market_type'] = talad_type
        stack.append(result)
        
        browser.close()
        time.sleep(4)
        
stack_result = pd.concat(stack)
stack_result = stack_result.reset_index().drop(columns = ['index'])

stack_result[stack_result.columns[0]] = stack_result[stack_result.columns[0]].astype(str)
stack_result[stack_result.columns[1]] = stack_result[stack_result.columns[1]].astype(float)
stack_result[stack_result.columns[2]] = stack_result[stack_result.columns[2]].astype(float)
stack_result[stack_result.columns[3]] = stack_result[stack_result.columns[3]].astype(str)
stack_result[stack_result.columns[4]] = stack_result[stack_result.columns[4]].astype(str)
stack_result[stack_result.columns[5]] = stack_result[stack_result.columns[5]].astype(str)
stack_result[stack_result.columns[6]] = pd.to_datetime(stack_result[stack_result.columns[6]], format='%d/%m/%Y')
stack_result[stack_result.columns[7]] = stack_result[stack_result.columns[7]].astype(str)

stack_result.to_csv(path + '/ข้อมูลราคาสินค้า_ตลาดไท.csv', index = 'False', encoding = 'utf-8')